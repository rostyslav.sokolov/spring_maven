create table people
(
    id          int auto_increment
        primary key,
    surname     varchar(255) null,
    name        varchar(255) null,
    patronymic  varchar(255) null,
    gender      varchar(255) null,
    data_birth  int          not null,
    num_pasport int          not null,
    address     varchar(255) null,
    ident_code  int          not null
);