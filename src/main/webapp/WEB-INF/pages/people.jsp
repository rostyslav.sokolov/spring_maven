<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>
<%@ page session="false" %>
<html>
<head>
    <title>People Page</title>

    <style type="text/css">
        .tg {
            border-collapse: collapse;
            border-spacing: 0;
            border-color: #ccc;
        }

        .tg td {
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #ccc;
            color: #333;
            background-color: #fff;
        }

        .tg th {
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #ccc;
            color: #333;
            background-color: #f0f0f0;
        }

        .tg .tg-4eph {
            background-color: #f9f9f9
        }
    </style>
</head>
<body>
<a href="../../index.jsp">Back to main menu</a>

<br/>
<br/>

<h1>People List</h1>

<c:if test="${!empty listPeople}">
    <table class="tg">
        <tr>
            <th width="80">ID</th>
            <th width="120">Surname</th>
            <th width="120">Name</th>
            <th width="120">Patronymic</th>
            <th width="120">Gender</th>
            <th width="120">Data_Birth</th>
            <th width="120">Num_pasport</th>
            <th width="120">Address</th>
            <th width="120">Ident_code</th>
            <th width="60">Edit</th>
            <th width="60">Delete</th>
        </tr>
        <c:forEach items="${listPeople}" var="people">
            <tr>
                <td>${people.id}</td>
                <td><a href="/peopledata/${people.id}" target="_blank">${people.surname}</a></td>
                <td>${people.name}</td>
                <td>${people.patronymic}</td>
                <td>${people.gender}</td>
                <td>${people.data_birth}</td>
                <td>${people.num_pasport}</td>
                <td>${people.address}</td>
                <td>${people.ident_code}</td>
                <td><a href="<c:url value='/edit/${people.id}'/>">Edit</a></td>
                <td><a href="<c:url value='/remove/${people.id}'/>">Delete</a></td>
            </tr>
        </c:forEach>
    </table>
</c:if>


<h1>Add a People</h1>

<c:url var="addAction" value="/people/add"/>

<form:form action="${addAction}" commandName="people">
    <table>
        <c:if test="${!empty people.surname}">
            <tr>
                <td>
                    <form:label path="id">
                        <spring:message text="ID"/>
                    </form:label>
                </td>
                <td>
                    <form:input path="id" readonly="true" size="8" disabled="true"/>
                    <form:hidden path="id"/>
                </td>
            </tr>
        </c:if>
        <tr>
            <td>
                <form:label path="surname">
                    <spring:message text="surname"/>
                </form:label>
            </td>
            <td>
                <form:input path="surname"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="name">
                    <spring:message text="name"/>
                </form:label>
            </td>
            <td>
                <form:input path="name"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="patronymic">
                    <spring:message text="patronymic"/>
                </form:label>
            </td>
            <td>
                <form:input path="patronymic"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="gender">
                    <spring:message text="gender"/>
                </form:label>
            </td>
            <td>
                <form:input path="gender"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="data_birth">
                    <spring:message text="data_birth"/>
                </form:label>
            </td>
            <td>
                <form:input path="data_birth"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="num_pasport">
                    <spring:message text="num_pasport"/>
                </form:label>
            </td>
            <td>
                <form:input path="num_pasport"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="address">
                    <spring:message text="address"/>
                </form:label>
            </td>
            <td>
                <form:input path="address"/>
            </td>
        </tr>
        <tr>
            <td>
                <form:label path="ident_code">
                    <spring:message text="ident_code"/>
                </form:label>
            </td>
            <td>
                <form:input path="ident_code"/>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <c:if test="${!empty people.surname}">
                    <input type="submit"
                           value="<spring:message text="Edit People"/>"/>
                </c:if>
                <c:if test="${empty people.surname}">
                    <input type="submit"
                           value="<spring:message text="Add People"/>"/>
                </c:if>
            </td>
        </tr>
    </table>
</form:form>
</body>
</html>
