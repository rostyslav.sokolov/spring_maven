
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>

<html>
<head>
  <title>PeopleData</title>

  <style type="text/css">
    .tg {
      border-collapse: collapse;
      border-spacing: 0;
      border-color: #ccc;
    }

    .tg td {
      font-family: Arial, sans-serif;
      font-size: 14px;
      padding: 10px 5px;
      border-style: solid;
      border-width: 1px;
      overflow: hidden;
      word-break: normal;
      border-color: #ccc;
      color: #333;
      background-color: #fff;
    }

    .tg th {
      font-family: Arial, sans-serif;
      font-size: 14px;
      font-weight: normal;
      padding: 10px 5px;
      border-style: solid;
      border-width: 1px;
      overflow: hidden;
      word-break: normal;
      border-color: #ccc;
      color: #333;
      background-color: #f0f0f0;
    }

    .tg .tg-4eph {
      background-color: #f9f9f9
    }
  </style>

</head>
<body>
<h1>People Details</h1>

<table class="tg">
  <tr>
    <td width="80">ID</td>
    <td width="120">Surname</td>
    <td width="120">Name</td>
    <td width="120">Patronymic</td>
    <td width="120">Gender</td>
    <td width="120">Data_Birth</td>
    <td width="120">Num_pasport</td>
    <td width="120">Address</td>
    <td width="120">Ident_code</td>
    <td width="60">Edit</td>
    <td width="60">Delete</td>
  </tr>
  <tr>
    <td>${people.id}</td>
    <td>${people.surname}</td>
    <td>${people.name}</td>
    <td>${people.patronymic}</td>
    <td>${people.gender}</td>
    <td>${people.data_birth}</td>
    <td>${people.num_pasport}</td>
    <td>${people.address}</td>
    <td>${people.ident_code}</td>
  </tr>
</table>
</body>
</html>