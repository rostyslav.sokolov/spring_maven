package net.physical.people.service;

import net.physical.people.dao.PeopleDao;
import net.physical.people.model.People;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PeopleServiceImpl implements PeopleService {
    private PeopleDao peopleDao;

    public void setPeopleDao(PeopleDao peopleDao) {
        this.peopleDao = peopleDao;
    }

    @Override
    @Transactional
    public void addPeople(People people) {
        this.peopleDao.addPeople(people);
    }

    @Override
    @Transactional
    public void updatePeople(People people) {
        this.peopleDao.updatePeople(people);
    }

    @Override
    @Transactional
    public void removePeople(int id) {
        this.peopleDao.removePeople(id);
    }

    @Override
    @Transactional
    public People getPeopleById(int id) {
        return this.peopleDao.getPeopleById(id);
    }

    @Override
    @Transactional
    public List<People> listPeople() {
        return this.peopleDao.listPeople();
    }
}