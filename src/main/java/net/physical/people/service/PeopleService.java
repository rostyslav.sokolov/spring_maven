package net.physical.people.service;

import net.physical.people.model.People;

import java.util.List;

public interface PeopleService {
    void addPeople(People people);

    void updatePeople(People people);

    void removePeople(int id);

    People getPeopleById(int id);

    List<People> listPeople();
}
