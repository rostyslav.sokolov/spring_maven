package net.physical.people.model;

import javax.persistence.*;

@Entity
@Table(name = "people")
public class People {
    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "SURNAME")
    private String surname;

    @Column(name = "NAME")
    private String name;

    @Column(name = "PATRONYMIC")
    private String patronymic;

    @Column(name = "GENDER")
    private String gender;

    @Column(name = "DATA_BIRTH")
    private int data_birth;

    @Column(name = "NUM_PASPORT")
    private int num_pasport;

    @Column(name = "ADDRESS")
    private String address;

    @Column(name = "IDENT_CODE")
    private int ident_code;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getData_birth() {
        return data_birth;
    }

    public void setData_birth(int data_birth) {
        this.data_birth = data_birth;
    }

    public int getNum_pasport() {
        return num_pasport;
    }

    public void setNum_pasport(int num_pasport) {
        this.num_pasport = num_pasport;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getIdent_code() {
        return ident_code;
    }

    public void setIdent_code(int ident_code) {
        this.ident_code = ident_code;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", gender='" + gender + '\'' +
                ", data_birth=" + data_birth +
                ", num_pasport=" + num_pasport +
                ", address='" + address + '\'' +
                ", ident_code=" + ident_code +
                '}';
    }
}
