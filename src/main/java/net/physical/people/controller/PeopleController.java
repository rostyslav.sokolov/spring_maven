package net.physical.people.controller;

import net.physical.people.model.People;
import net.physical.people.service.PeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PeopleController {
    private PeopleService peopleService;

    @Autowired()
    @Qualifier(value = "peopleService")
    public void setPeopleService(PeopleService peopleService) {
        this.peopleService = peopleService;
    }

    @RequestMapping(value = "people", method = RequestMethod.GET)
    public String listPeople(Model model){
        model.addAttribute("people", new People());
        model.addAttribute("listPeople", this.peopleService.listPeople());

        return "people";
    }

    @RequestMapping(value = "/people/add", method = RequestMethod.POST)
    public String addPeople(@ModelAttribute("people") People people){
        if(people.getId() == 0){
            this.peopleService.addPeople(people);
        }else {
            this.peopleService.updatePeople(people);
        }

        return "redirect:/people";
    }

    @RequestMapping("/remove/{id}")
    public String removePeople(@PathVariable("id") int id){
        this.peopleService.removePeople(id);

        return "redirect:/people";
    }

    @RequestMapping("edit/{id}")
    public String editPeople(@PathVariable("id") int id, Model model){
        model.addAttribute("people", this.peopleService.getPeopleById(id));
        model.addAttribute("listPeople", this.peopleService.listPeople());

        return "people";
    }

    @RequestMapping("peopledata/{id}")
    public String peopleData(@PathVariable("id") int id, Model model){
        model.addAttribute("people", this.peopleService.getPeopleById(id));

        return "peopledata";
    }
}
