package net.physical.people.dao;

import net.physical.people.model.People;

import java.util.List;

public interface PeopleDao {
    void addPeople(People people);

    void updatePeople(People people);

    void removePeople(int id);

    People getPeopleById(int id);

    List<People> listPeople();
}
