package net.physical.people.dao;

import net.physical.people.model.People;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PeopleDaoImpl implements PeopleDao {
    private static final Logger logger = LoggerFactory.getLogger(PeopleDaoImpl.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addPeople(People people) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(people);
        logger.info("People successfully saved. details: " + people);
    }

    @Override
    public void updatePeople(People people) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(people);
        logger.info("People successfully update.details: " + people);
    }

    @Override
    public void removePeople(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        People people = (People) session.load(People.class, id);

        if(people !=null){
            session.delete(people);
        }
        logger.info("People successfully removed.details: " + people);
    }

    @Override
    public People getPeopleById(int id) {
        Session session =this.sessionFactory.getCurrentSession();
        People people = (People) session.load(People.class, id);
        logger.info("People successfully loaded. details: " + people);

        return people;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<People> listPeople() {
        Session session = this.sessionFactory.getCurrentSession();
        List<People> peopleList = session.createQuery("from People").list();

        for(People people : peopleList){
            logger.info("People list: " + people);
        }

        return peopleList;
    }
}
